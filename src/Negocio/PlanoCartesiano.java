/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Funcion;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madar
 */
public class PlanoCartesiano {
    
    private Funcion funciones[];
    private String urlPlano;

    public PlanoCartesiano() {
    }

    public PlanoCartesiano(String urlPlano) {
        this.urlPlano = urlPlano;
        
        ArchivoLeerURL archivo=new ArchivoLeerURL(this.urlPlano);
        //Obtiene toda la información del archivo en un vector de object
        Object datos[]=archivo.leerArchivo();
        //Crear los espacios de memoria para el arreglo funciones
        this.funciones=new Funcion[datos.length-1];
        int i=0;
        for(int fila=1;fila<datos.length;fila++)
        {
            String datosFilas=datos[fila].toString();
            this.funciones[i]=new Funcion(datosFilas);
            i++;
        }
           
    }

    public Funcion[] getFunciones() {
        return funciones;
    }

    public void setFunciones(Funcion[] funciones) {
        this.funciones = funciones;
    }

    public String getUrlPlano() {
        return urlPlano;
    }

    public void setUrlPlano(String urlPlano) {
        this.urlPlano = urlPlano;
    }

    @Override
    public String toString() {
        String msg="";
        for(Funcion funcion:this.funciones)
        {
            msg+=funcion.toString()+"\n";
        }
        return msg;
    }
    
    
    /**
     * Ordena las funciones por cantidad de puntos ; es decir, primero almacena las funciones con 1ptos, 2ptos....n_ptos
     * para esto utiliza el método de inserción
     * Más información: 
     * https://juncotic.com/ordenamiento-por-insercion-algoritmos-de-ordenamiento/#:~:text=El%20algoritmo%20de%20ordenamiento%20por%20inserci%C3%B3n%20es%20un%20algoritmo%20de,insert%C3%A1ndolo%20en%20el%20lugar%20correspondiente.
     * Advertencia: IMPLEMENTAR COMPARETO EN LA CLASE FUNCION
     */
    
    public void ordenarFunciones()
    {
            // :)
    }
    
    
    
}
